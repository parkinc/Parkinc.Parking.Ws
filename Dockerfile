FROM node:latest
COPY app /app
COPY node_modules /node_modules
WORKDIR /app
EXPOSE 8080/tcp
ENTRYPOINT ["node", "index.js"]
