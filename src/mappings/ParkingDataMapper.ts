import { ParkingDataResponse } from "../models/ParkingDataResponse";
import { ParkingPlaceMapper } from "./ParkingPlaceMapper";

export class ParkingDataMapper {
    public static fromMessageData(msgData: any): ParkingDataResponse {
        console.log('MSG DATA', msgData);
        
        let data = new ParkingDataResponse();
        data.timestampUtc = new Date(msgData.TimestampUtc);
        data.parkingData = msgData.ParkingData.map((msgPlace) => ParkingPlaceMapper.fromMessageData(msgPlace));
        return data;
    }
}