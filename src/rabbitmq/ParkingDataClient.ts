import * as Amqp from 'amqp-ts';
import { RabbitMQConfig } from './RabbitMQConfig';
import { ParkingDataResponse } from '../models/ParkingDataResponse';
import { ParkingDataMapper } from '../mappings/ParkingDataMapper';
import { Guid } from 'guid-typescript';

export class ParkingDataClient {
    private _connection: Amqp.Connection;
    private _reqExchange: Amqp.Exchange;
    private _resQueue: Amqp.Queue;

    private static REQ_EXCHANGE_NAME = 'E_ParkingDataRequest';
    private static RES_EXCHANGE_NAME = 'E_ParkingDataResponse';
    private static RES_QUEUE_NAME_BASE = 'Q_ParkingDataResponse_'


    constructor(config: RabbitMQConfig, private _clientHash: string) {
        console.log('PDC constructor');
        console.log('config', config);
        console.log('hash', _clientHash);
        this.connect(config);
        this.initReqExchange();
        this.initResQueue();
    }


    public subscribe(onData: (data: ParkingDataResponse) => any) {
        const onMessage = (msg: Amqp.Message) => {
            const dataObj = JSON.parse(msg.getContent());
            const data = ParkingDataMapper.fromMessageData(dataObj);
            msg.ack();
            return onData(data);
        }
        this._resQueue.activateConsumer(onMessage);
    }

    public request() {
        let reqExchange = this._reqExchange;
        let msg = new Amqp.Message({});
        msg.properties["type"] = "Parkinc.Parking.Messages.ParkingDataRequestV1:Parkinc.Parking.Messages";
        msg.properties["correlation_id"] = Guid.create();
        console.log("message constructed");
        this._connection.completeConfiguration().then(() => { console.log("sending msg"); reqExchange.send(msg, '#') });
    }

    private connect(config: RabbitMQConfig) {
        const { user, password, host, port, vhost } = config;
        let uri = `amqp://${user}:${password}@${host}:${port}/${vhost}`;
        console.log('uri', uri);
        this._connection = new Amqp.Connection(uri);
        console.log('Connection set', this._connection);
    }

    private initReqExchange() {
        const { REQ_EXCHANGE_NAME } = ParkingDataClient
        this._reqExchange = this._connection.declareExchange(REQ_EXCHANGE_NAME, 'topic');
    }


    private initResQueue() {
        const { RES_QUEUE_NAME_BASE, RES_EXCHANGE_NAME } = ParkingDataClient;
        const queueName = `${RES_QUEUE_NAME_BASE}_${this._clientHash}`;
        this._resQueue = this._connection.declareQueue(queueName);

        const resExchange = this._connection.declareExchange(RES_EXCHANGE_NAME, 'topic');
        this._resQueue.bind(resExchange);
    }
}