export class ParkingPlace {
    private _name: string;
    private _latitude: number;
    private _longitude: number;
    private _freeCount: number;
    private _maxCount: number;
    private _isOpen: boolean;
    private _isPaymentActive: boolean;
    private _status: number;

	public get name(): string {
		return this._name;
	}

	public set name(value: string) {
		this._name = value;
	}

	public get latitude(): number {
		return this._latitude;
	}

	public set latitude(value: number) {
		this._latitude = value;
	}

	public get longitude(): number {
		return this._longitude;
	}

	public set longitude(value: number) {
		this._longitude = value;
	}

	public get freeCount(): number {
		return this._freeCount;
	}

	public set freeCount(value: number) {
		this._freeCount = value;
	}
    

	public get maxCount(): number {
		return this._maxCount;
	}

	public set maxCount(value: number) {
		this._maxCount = value;
	}

	public get isOpen(): boolean {
		return this._isOpen;
	}

	public set isOpen(value: boolean) {
		this._isOpen = value;
	}

	public get isPaymentActive(): boolean {
		return this._isPaymentActive;
	}

	public set isPaymentActive(value: boolean) {
		this._isPaymentActive = value;
	}

	public get status(): number {
		return this._status;
	}

	public set status(value: number) {
		this._status = value;
	}
    
    
}