export class RabbitMQConfig {

    constructor(private _host: string, private _vhost: string, 
                private _port: number, private _user: string, 
                private _password: string) {}


    
    public get host() : string {
        return this._host;
    }
    public get vhost() : string {
        return this._vhost;
    }
    public get port() : number {
        return this._port;
    }
    public get user() : string {
        return this._user;
    }
    public get password() : string {
        return this._password;
    }
    
}