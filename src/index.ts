import {ClientHash} from './utils/ClientHash';
import { ParkingDataClient } from "./rabbitmq/ParkingDataClient";
import { RabbitMQConfig } from "./rabbitmq/RabbitMQConfig";
import { ParkingDataResponse } from './models/ParkingDataResponse';
import { ParkingWsServer } from './server';


let config = new RabbitMQConfig("swan.rmq.cloudamqp.com", "naghkhhc", 5672, "naghkhhc", "uRHTZEgVK7KZKcvrXe1WjgQpqUBK1y_M");
let hash = 'ParkingWSProxy'
let client = new ParkingDataClient(config, hash);

let server = new ParkingWsServer();

server.addHandler('parkingDataRequest', () => { console.log('request recieved'); client.request()});

client.subscribe((data: ParkingDataResponse) => {
    console.log('Broadcasting parking data..');
    server.broadcast('parkingData', data);
});