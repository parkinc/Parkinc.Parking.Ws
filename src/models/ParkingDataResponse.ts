import { ParkingPlace } from "./ParkingPlace";

export class ParkingDataResponse {

    private _timestampUtc: Date;

	public get timestampUtc(): Date {
		return this._timestampUtc;
	}

	public set timestampUtc(value: Date) {
		this._timestampUtc = value;
	}

	public get parkingData(): ParkingPlace[] {
		return this._parkingData;
	}

	public set parkingData(value: ParkingPlace[]) {
		this._parkingData = value;
	}
    private _parkingData: ParkingPlace[];

}