import { createServer, Server } from 'http'
import * as express from 'express'
import * as socketIo from 'socket.io'
import { Guid } from 'guid-typescript'


export class ParkingWsServer {

    public static readonly PORT:number = 8080;
    private _app: express.Application;
    private _server: Server;
    private _io: SocketIO.Server;
    private _port: string | number;
    private _handlers: ({ event: string, handler: (...args: any[]) => any})[];

    constructor() {
        this.createApp();
        this.configure();
        this.createServer();
        this.initSockets();
        this.listen();
        this._handlers = [];
    }

    private createApp(): void {
        this._app = express();
    }

    private createServer(): void {
        this._server = createServer(this._app);
    }

    private configure(): void {
        this._port = process.env.PORT || ParkingWsServer.PORT;
    }

    private initSockets(): void {
        this._io = socketIo(this._server);
    }

    private listen(): void {
        this._server.listen(this._port, () => {
            console.log(`Server started on port ${this._port}`)
        });
        this._io.on('connect', (socket: SocketIO.Socket) => {
            let clientId = Guid.create();
            console.log(`Client connected (ID: ${clientId})`);

            this._handlers.forEach( handlerObj => {
                let { event, handler } = handlerObj;
                console.log(`Registering handler for '${event}'`);
                socket.on(event, handler);
            });
        })
    }

    public addHandler(event: string, handler: (...args: any[]) => any) {
        this._handlers.push({ event, handler});
    }

    public broadcast(event: string, message: any) {
        console.log('Event emitted: ', event);
        console.log(this._io.sockets.clients);
        this._io.sockets.emit(event, message);
    }



}

