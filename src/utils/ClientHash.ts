import { Guid } from 'guid-typescript';
import { Buffer } from 'buffer';


export class ClientHash {
    public static create(): string {
        const buffer = new Buffer(Guid.create().toString());
        const base64 = buffer.toString('base64');
        return base64.replace(/[/+=]/g, '');
    }
}
