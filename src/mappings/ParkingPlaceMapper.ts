import { ParkingPlace } from "../models/ParkingPlace";

export class ParkingPlaceMapper {
    public static fromMessageData(msgPlace: any): ParkingPlace {
        let place = new ParkingPlace();
        place.name = msgPlace.Name;
        place.latitude = msgPlace.Latitude;
        place.longitude = msgPlace.Longitude;
        place.freeCount = msgPlace.FreeCount;
        place.maxCount = msgPlace.MaxCount;
        place.isOpen = msgPlace.IsOpen;
        place.isPaymentActive = msgPlace.IsPaymentActive;
        place.status = msgPlace.Status;
        return place;
    }
}